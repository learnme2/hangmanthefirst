import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.net.URL;

public class HangmanGUI extends JFrame {
    private String guessWord;
    private char[] guessedLetters;

    private JLabel wordLabel;
    private JTextField letterField;
    private JButton guessButton;
    private JLabel imageLabel;

    private int attemptsLeft;

    public HangmanGUI() {
        super("Hangman Game");
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        guessWord = JOptionPane.showInputDialog("Enter the word to guess");

        if (guessWord != null && !guessWord.isEmpty()) {
            guessedLetters = new char[guessWord.length()];

            for (int i = 0; i < guessedLetters.length; i++) {
                guessedLetters[i] = '_';
            }

            wordLabel = new JLabel(getFormattedWord());
            letterField = new JTextField(1);
            guessButton = new JButton("Guess");

            guessButton.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    handleGuess();
                }
            });

            imageLabel = new JLabel();
            imageLabel.setHorizontalAlignment(JLabel.CENTER);
            imageLabel.setPreferredSize(new Dimension(300, 300));

            JPanel panel = new JPanel();
            panel.setLayout(new BorderLayout());

            JPanel topPanel = new JPanel();
            topPanel.setLayout(new FlowLayout());
            topPanel.add(wordLabel);
            topPanel.add(letterField);
            topPanel.add(guessButton);

            panel.add(topPanel, BorderLayout.NORTH);
            panel.add(imageLabel, BorderLayout.CENTER);

            add(panel);

            pack();
            setLocationRelativeTo(null);
            setVisible(true);

            attemptsLeft = 6;
        } else {
            System.out.println("No word entered. Exiting.");
            System.exit(0);
        }
    }

    private String getFormattedWord() {
        return String.valueOf(guessedLetters);
    }

    private void handleGuess() {
        String input = letterField.getText();

        if (input.length() == 1 && Character.isLetter(input.charAt(0))) {
            char letter = input.charAt(0);


            letter = Character.toLowerCase(letter);

            boolean rightLetter = false;
            for (int i = 0; i < guessWord.length(); i++) {

                if (Character.toLowerCase(guessWord.charAt(i)) == letter && guessedLetters[i] == '_') {
                    guessedLetters[i] = Character.toUpperCase(letter);
                    rightLetter = true;
                }
            }

            if (!rightLetter) {
                attemptsLeft--;
                updateImage();
            }

            updateUI();

            if (isWordGuessed()) {

                showWinMessage();
            } else if (attemptsLeft == 0) {

                showLoseMessage();
            }

        } else {
            JOptionPane.showMessageDialog(this, "Please enter a valid letter.", "Invalid Input", JOptionPane.ERROR_MESSAGE);
        }

        letterField.setText("");
    }


    private boolean isWordGuessed() {
        for (char gl : guessedLetters) {
            if (gl == '_') {
                return false;
            }
        }
        return true;
    }

    private void updateUI() {
        wordLabel.setText(getFormattedWord());
        setTitle("Attempts left: " + attemptsLeft);
    }

    private void updateImage() {
        try {
            String imageName = "error" + (attemptsLeft) + ".jpg";

            URL imageURL = getClass().getClassLoader().getResource("resources/" + imageName);
            if (imageURL != null) {
                ImageIcon icon = new ImageIcon(imageURL);
                imageLabel.setIcon(icon);
            } else {
                System.out.println("Error: Unable to load image: " + imageName);
            }
        } catch (Exception e) {
            e.printStackTrace();
            JOptionPane.showMessageDialog(this, "Error loading image", "Image Error", JOptionPane.ERROR_MESSAGE);
        }
    }

    private void showWinMessage() {
        int option = JOptionPane.showConfirmDialog(this, "Congratulations, you guessed the word! Do you want to play again?", "You Win!", JOptionPane.YES_NO_OPTION, JOptionPane.INFORMATION_MESSAGE);

        if (option == JOptionPane.YES_OPTION) {
            resetGame();
        } else {
            System.exit(0);
        }
    }

    private void showLoseMessage() {
        int option = JOptionPane.showConfirmDialog(this, "Sorry, you couldn't guess the word. Do you want to play again?", "Game Over", JOptionPane.YES_NO_OPTION, JOptionPane.ERROR_MESSAGE);

        if (option == JOptionPane.YES_OPTION) {
            resetGame();
        } else {
            System.exit(0);
        }
    }

    private void resetGame() {
        HangmanGUI newGame = new HangmanGUI();
        dispose();
    }


}
