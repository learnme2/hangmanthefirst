import java.util.Scanner;

public class HangmanMechanics {
    public static void playHangman() {
        Scanner scanner = new Scanner(System.in);

        System.out.print("Please enter the word to guess: ");

        String guessWord = scanner.nextLine().toLowerCase();
        char[] guessedLetters = new char[guessWord.length()];

        for (int i = 0; i < guessedLetters.length; i++) {
            guessedLetters[i] = '_';
        }

        int maxAttempts = 6;
        int countToHang = 0;

        while (maxAttempts > 0) {
            System.out.println("Word: " + String.valueOf(guessedLetters));
            System.out.println("Attempts left: " + maxAttempts);
            System.out.print("Please enter next letter to guess: ");
            char letter = scanner.next().toLowerCase().charAt(0);

            boolean rightLetter = false;
            for (int i = 0; i < guessWord.length(); i++) {
                if (guessWord.charAt(i) == letter) {
                    guessedLetters[i] = letter;
                    rightLetter = true;
                }
            }

            if (!rightLetter){
                maxAttempts--;
                hangmanDrawing(maxAttempts);
            }

            boolean weHaveGuessedEverything = true;
            for (char gl : guessedLetters) {
                if (gl == '_') {
                    weHaveGuessedEverything = false;
                    break;
                }
            }

            if (weHaveGuessedEverything) {
                System.out.println("Congratulations, you have guessed everything right! It was in fact: " + guessWord);
                break;
            }
        }

        if (maxAttempts == 0) {
            System.out.println("You lost the game! The answer in this game was: " + guessWord);
        }

        scanner.close();
    }
    public static void hangmanDrawing(int wrongGuess) {
        switch (wrongGuess) {
            case 0:
                System.out.println("  ____");
                System.out.println(" |    |");
                System.out.println(" |    O");
                System.out.println(" |   /|\\");
                System.out.println(" |   / \\");
                System.out.println(" |");
                break;
            case 1:
                System.out.println("  ____");
                System.out.println(" |    |");
                System.out.println(" |    O");
                System.out.println(" |   /|\\");
                System.out.println(" |   /");
                System.out.println(" |");
                break;
            case 2:
                System.out.println("  ____");
                System.out.println(" |    |");
                System.out.println(" |    O");
                System.out.println(" |   /|\\");
                System.out.println(" |");
                System.out.println(" |");
                break;
            case 3:
                System.out.println("  ____");
                System.out.println(" |    |");
                System.out.println(" |    O");
                System.out.println(" |   /|");
                System.out.println(" |");
                System.out.println(" |");
                break;
            case 4:
                System.out.println("  ____");
                System.out.println(" |    |");
                System.out.println(" |    O");
                System.out.println(" |    |");
                System.out.println(" |");
                System.out.println(" |");
                break;
            case 5:
                System.out.println("  ____");
                System.out.println(" |    |");
                System.out.println(" |    O");
                System.out.println(" |");
                System.out.println(" |");
                System.out.println(" |");
                break;
        }
    }
}
