import javax.swing.*;
import java.util.Scanner;

public class Hangman {
    public static void main(String[] args) {
        SwingUtilities.invokeLater(() -> {
            HangmanGUI hangmanGUI = new HangmanGUI();
        });
        HangmanMechanics.playHangman();
    }

}
